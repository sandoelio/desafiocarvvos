<?php
error_reporting(E_ALL ^ E_NOTICE);
require_once("mostra-alerta.php"); ?>

<html>
<head>
    <title>Minha loja</title>
    <meta charset="utf-8">
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/loja.css" rel="stylesheet" />
</head>
<body>
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a href="login.php" class="navbar-brand">Login</a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li><a href="index.php">Cadastramento</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container">

    <div class="principal">
<?php
        mostraAlerta("success");
        mostraAlerta("danger");
?>