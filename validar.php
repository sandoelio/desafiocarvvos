<?php require_once("cabecalho.php");
      require_once("conecta.php");
      require_once("banco-cadastro.php");   
?>

<?php
	
//verificando se a entrada de dados estar vazia ao INSERIR
if (!empty($_POST)) {	
	$formValido = true;

	if (trim($_POST["nome"])==""){
		echo "* o campo nome em branco <br/>";
		$formValido = false;
	}

	if (trim($_POST["sobrenome"])==""){
		echo "* o campo sobrenome em branco <br/>";
		$formValido = false;
	}

	if (trim($_POST["cpf"])==""){
		echo "* o campo cpf em branco <br/>";
		$formValido = false;
	}

	if (trim($_POST["telefone"])==""){
		echo "* o campo telefone em branco <br/>";
		$formValido = false;
	}

	if (trim($_POST["email"])==""){
		echo "* o campo email em branco <br/>";
		$formValido = false;
	}

	if (trim($_POST["senha"])==""){
		echo "* o campo senha em branco <br/>";
		$formValido = false;
	}
	if (trim($_POST["numero"])==""){
		echo "* o campo numero em branco <br/>";
		$formValido = false;
	}
	//Verifica se os campos senhas estao iguais
	if ($_POST["senha"] != $_POST["repetSenha"]){
		echo $mensagem = "<span class='erro'><b>Erro</b>: A senha não conferem!</span>";
	die();
	}
	//Verifica se os campos email estao iguais
	if ($_POST["email"] != $_POST["repetEmail"]){
		echo $mensagem = "<span class='erro'><b>Erro</b>: O email não conferem!</span>";
	die();
	}

	//inserindo os dados nas variaveis
	if ($formValido) {
		$nome      =trim($_POST['nome']);
		$sobrenome =trim($_POST['sobrenome']);
		$cpf       =trim($_POST['cpf']);
		$telefone  =trim($_POST['telefone']);
		$email     =trim($_POST['email']);
		$senha     =trim($_POST['senha']);
		$cep       =trim($_POST['cep']);
		$estado    =trim($_POST['estado']);
		$cidade    =trim($_POST['cidade']);
		$rua       =trim($_POST['rua']);
		$numero    =trim($_POST['numero']);
		$bairro    =trim($_POST['bairro']);

		insereDados($conexao,$nome,$sobrenome,$cpf,$telefone,$email,$senha,$cep,$estado,$cidade,$rua,$numero,$bairro)?>
    	<p class="text-success">A pessoa <?= $nome; ?> <?= $sobrenome; ?> adicionado com sucesso!</p>
<?php } else {
    	$msg = mysqli_error($conexao);
?>
    	<p class="text-danger">A pessoa <?= $nome; ?> <?= $sobrenome; ?> não foi adicionado: <?= $msg ?></p>
<?php
		}
	}
?>
	
<?php require_once("rodape.php"); ?>