<?php
require_once("cabecalho.php");
require_once("logica-usuario.php");
?>

<?php if(usuarioEstaLogado()) { ?>
    <p class="text-success">Você está logado como <?= usuarioLogado() ?>. <a href="logout.php">Deslogar</a></p>
<?php } else { ?>

	<h2>Login</h2>
	<form action="validaLogin.php" method="post">
		<table class="table">
			<tr>
				<td>Email ou CPF</td>
				<td><input type="text" class="form-control" id="email" name="email"></td>
			</tr>
			<tr>
				<td>Senha</td>
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

				<td><input type="password" class="form-control" id="senha" name="senha"></td>
				<td><img id="olho" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABDUlEQVQ4jd2SvW3DMBBGbwQVKlyo4BGC4FKFS4+TATKCNxAggkeoSpHSRQbwAB7AA7hQoUKFLH6E2qQQHfgHdpo0yQHX8T3exyPR/ytlQ8kOhgV7FvSx9+xglA3lM3DBgh0LPn/onbJhcQ0bv2SHlgVgQa/suFHVkCg7bm5gzB2OyvjlDFdDcoa19etZMN8Qp7oUDPEM2KFV1ZAQO2zPMBERO7Ra4JQNpRa4K4FDS0R0IdneCbQLb4/zh/c7QdH4NL40tPXrovFpjHQr6PJ6yr5hQV80PiUiIm1OKxZ0LICS8TWvpyyOf2DBQQtcXk8Zi3+JcKfNafVsjZ0WfGgJlZZQxZjdwzX+ykf6u/UF0Fwo5Apfcq8AAAAASUVORK5CYII="/></td>
			</tr>

			<tr>
				<td><button class="btn btn-primary">Login</button></td>
			</tr>
		</table>
	</form>
<?php }?>

<script>
	// script para o icone de mostrar e ocultar senha 
	var senha = $('#senha');
	var olho= $("#olho");

	olho.mousedown(function() {
	  senha.attr("type", "text");
	});

	olho.mouseup(function() {
	  senha.attr("type", "password");
	});
	// para evitar o problema de arrastar a imagem e a senha continuar exposta, 
	//citada pelo nosso amigo nos comentários
	$( "#olho" ).mouseout(function() { 
	  $("#senha").attr("type", "password");
	});
</script>
<?php include("rodape.php"); ?>