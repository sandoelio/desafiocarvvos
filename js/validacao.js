<script>  
   function mascaraTel(){
       var telefone= document.getElementById('telefone').value;
       if(telefone.length==1){
        document.getElementById('telefone').value ='(' + telefone;
         }
      else if (telefone.length==3){
        document.getElementById('telefone').value = telefone +')';
       }
       else if (telefone.length==9){
        document.getElementById('telefone').value = telefone +'-';
       }
}

   function mascaraCpf(){
       var cpf = document.getElementById('cpf').value;
        if(cpf.length==3){
         document.getElementById('cpf').value = cpf +'.';
    }
        else if(cpf.length==7){
         document.getElementById('cpf').value = cpf +'.';
    }
        else if (cpf.length==11){
         document.getElementById('cpf').value = cpf +'-';
    }
}  

function checarEmail(){
   var email= document.getElementById('email').value;
   if(email==""||email.indexOf('@')==-1||email.indexOf('.')==-1){

    alert("Por favor Insira um E-mail valido");

    }
}

function limpa_formulário_cep() {

    document.getElementById('rua').value=("");
    document.getElementById('bairro').value=("");
    document.getElementById('cidade').value=("");
    document.getElementById('estado').value=("");
    }
 function meu_callback(conteudo) {
    if (!("erro" in conteudo)) {
        //Atualiza os campos com os valores.
        document.getElementById('rua').value=(conteudo.logradouro);
        document.getElementById('bairro').value=(conteudo.bairro);
        document.getElementById('cidade').value=(conteudo.localidade);
        document.getElementById('estado').value=(conteudo.uf);
    } 
    else {

        limpa_formulário_cep();
        alert("CEP não encontrado.");
    }
    }

 function mascaraCep(){
   var cep = document.getElementById('cep').value;
    if(cep.length==5){
     document.getElementById('cep').value = cep +'-';
    }
}
 function pesquisacep(valor) {
    var cep = valor.replace(/\D/g, '');

    if (cep != "") {

        var validacep = /^[0-9]{8}$/;

        if(validacep.test(cep)) {

            //Preenche os campos com "..." enquanto consulta webservice.
            document.getElementById('rua').value="...";
            document.getElementById('bairro').value="...";
            document.getElementById('cidade').value="...";
            document.getElementById('estado').value="...";

            var script = document.createElement('script');

            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

            document.body.appendChild(script);

        } 
        else {

            limpa_formulário_cep();
            alert("Formato de CEP inválido.");
        }
    }
    else {

        limpa_formulário_cep();
    }
}

//função para validar cpf
function is_cpf (cpf) {

  if((cpf = cpf.replace(/[^\d]/g,"")).length != 11)
    return false
//verificando se os digitos sao iguais
  if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || 
      cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" ||
      cpf == "88888888888" || cpf == "99999999999")
    return false;

  var resto;
  var soma = 0;

  for (i=1; i<=9; i++)
    soma = soma + parseInt(cpf[i-1]) * (11 - i);
    resto = (soma * 10) % 11;

  if ((resto == 10) || (resto == 11))
    resto = 0;

  if (resto != parseInt(cpf[9]))
    return false;

  soma = 0;

  for (i = 1; i <= 10; i++)
    soma = soma + parseInt(cpf[i-1]) * (12 - i);

  resto = (soma * 10) % 11;

  if ((resto == 10) || (resto == 11))
    resto = 0;

  if (resto != parseInt(cpf[10]))
    return false;

  return true;
}


function fMasc(objeto,mascara) {
obj=objeto
masc=mascara
setTimeout("fMascEx()",1)
}

  function fMascEx() {
obj.value=masc(obj.value)
}

function mCPF(cpf){
cpf=cpf.replace(/\D/g,"")
cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
return cpf
}

cpfCheck = function (param) {
    document.getElementById('cpfResponse').innerHTML = is_cpf(param.value)? '<span style="color:green">válido</span>' : '<span style="color:red">inválido</span>';
    if(param.value=='') document.getElementById('cpfResponse').innerHTML = '';
}
</script>
