<?php 
require_once("banco-cadastro.php");
require_once("logica-usuario.php");

$usuario = buscaUsuario($conexao, $_POST["email"], $_POST["cpf"], $_POST["senha"]);

if($usuario == null ) {
    $_SESSION["danger"] = "Usuário ou senha inválido.";
    header("Location: login.php");
} else {
    $_SESSION["success"] = "Usuário logado com sucesso.";
    logaUsuario($usuario["email"]);
    header("Location: login.php");
}
die();
?>