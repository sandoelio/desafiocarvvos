<?php require_once("cabecalho.php");
      require_once("conecta.php");
      require_once("js/validacao.js");
      require_once("banco-cadastro.php");
?>

<h1>Formulário de cadastro</h1>
<form action="validar.php" method="post">
<fieldset>
 <legend>Dados Pessoais</legend>
<table>
    <tr>
        <td>
            <label>CPF:</label>
        </td>
        <td>
            <input type="text" class="form-control" id="cpf" onkeyup="cpfCheck(this)"maxlength="14" placeholder="999.999.999-99" name="cpf" onkeydown="javascript:fMasc( this, mCPF );">
        </td>
        <td>
             <span id="cpfResponse"></span>
        </td>
    </tr>
</form>
    <tr>
        <td>
            <label>Nome: </label>
        </td>
        <td>
            <input type="text" class="form-control " name="nome" id="nome" size="40" maxlength="40">
        </td>
    </tr>
    <tr>
        <td>
            <label>Sobrenome: </label>
        </td>
        <td>
            <input type="text" class="form-control " name="sobrenome" id="sobrenome" size="40" maxlength="40">
        </td>
    </tr>
    <tr>
        <td>

            <label>Telefone:</label>
        </td>
        <td>
           <input type="text" class="form-control" id="telefone" onkeypress="mascaraTel()" maxlength="14" placeholder="(99)99999-9999" name="telefone">
        </td>
    </tr>
    <tr>
        <td>
            <label>Email:</label>
        </td>
        <td>
            <input type="text" class="form-control " id="email" onblur="checarEmail()" name="email" placeholder="nome@email.com"> 
        </td>
    <tr>
        <td>
            <label>Repetir email:</label>
        </td>
        <td>
            <input type="email" class="form-control " name="repetEmail" id="repetEmail" placeholder="nome@email.com"> 
        </td>
    </tr>
    <tr>
        <td>
            <label>Senha:</label>
        </td>
        <td>
            <input type="text" class="form-control " id="senha" name="senha">
        </td>
    </tr>
    <tr>
        <td>
            <label>Repetir senha:</label>
        </td>
        <td>
            <input type="text" class="form-control " id="repetSenha" name="repetSenha">
        </td>
    </tr>
</table>
</fieldset>

<br />
<br />

<fieldset>
<legend>Dados de Endereço</legend>
<table>
    <tr>
        <td>
            <label>CEP: </label>
        </td>
        <td>
            <input type="text" class="form-control" id="cep" onblur="pesquisacep(this.value)" onkeypress="mascaraCep()" name="cep" placeholder="99999-999"
             size="9" maxlength="9">
        </td>
    </tr>
    <tr>
        <td>
            <label>Estado:</label>
        </td>
        <td>
            <input type="text" class="form-control " id="estado" name="estado">
        </td>
    </tr>
    <tr>
        <td>
            <label>Cidade:</label>
        </td>
        <td>
            <input type="text" class="form-control " id="cidade" name="cidade">
        </td>
    </tr>
    <tr>
        <td>
            <label>Rua:</label>
        </td>
        <td>
            <input type="text" class="form-control " name="rua" id="rua">
        </td>
    </tr>
    <tr>
        <td>
            <label>Numero:</label>
        </td>
        <td>
            <input type="text" class="form-control " id="numero" name="numero" size="4" maxlength="4">
        </td>
    </tr>
    <tr>
        <td>
            <label>Bairro: </label>
        </td>
        <td>
            <input type="text" class="form-control" id="bairro" name="bairro">
        </td>
    </tr>              
</table>
</fieldset>
<br />
    <tr>
        <td>
            <button class="btn btn-primary" type="submit" id="btnSubmit">Cadastrar</button>
        </td>
    </tr>

<?php require_once("rodape.php"); ?>
